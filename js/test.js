/* global describe, it, expect, beforeEach, afterEach, afterAll,
sinon, jasmine, validarNombre, myLib */

describe('Validar Nombre', () => {
  it('Existe la función', () => {
    expect(validarNombre).toEqual(jasmine.any(Function))
  })
  it('No acepta cadena vacía', () => {
    expect(validarNombre('')).toEqual(false)
  })
  it('Cada palabra tiene máximo 30 caracteres', () => {
    const prueba = 'SuperCaliFragilisticoEspiralidoso Aunque al decirlo todo suene enredoso!'
    expect(validarNombre(prueba)).toEqual(false)
  })
  it('Debe tener mayúsculas y minúsculas', () => {
    const soloMinusculas = 'nombre falso'
    const soloMayusculas = 'NOMBRE GRITONEADO'
    expect(validarNombre(soloMinusculas)).toEqual(false)
    expect(validarNombre(soloMayusculas)).toEqual(false)
  })
  it('Mínimo 2 palabras', () => {
    const unaPalabra = 'Juanito'
    expect(validarNombre(unaPalabra))
      .toEqual(false)
    const dosPalabras = 'La Bikina'
    expect(validarNombre(dosPalabras))
      .toEqual(true)
    const trampa = 'Khé?   '
    expect(validarNombre(trampa))
      .toEqual(false)
  })
  it('No empieza con espacios', () => {
    const fechaAhorita = Date.now()
    const nombreRandom = ` ${fechaAhorita}`
    expect(validarNombre(nombreRandom)).toEqual(false)
  })
})

describe('Probar entradas de nombre', () => {
  let inputNombre
  let submit
  let result

  beforeEach(() => {
    inputNombre = document.querySelector('#name')
    inputNombre.value = ''
    submit = document.querySelector('input[type="submit"]')
    result = document.querySelector('#validationResult')
  })
  it('No acepta nombre de una "palabra"', () => {
    inputNombre.value = Date.now()
    submit.click()
    expect(result.className).toEqual('bad')
  })

  it('Acepta nombre con todos los requisitos', () => {
    inputNombre.value = 'Aurora Boreal'
    submit.click()
    expect(result.className).toEqual('good')
  })

  afterAll(() => {
    [...document.body.querySelectorAll('form'), ...document.body.querySelectorAll('#validationResults')]
      .forEach(e => e.remove())
  })
})

describe('XML HTTP Request fake server test', () => {
  let server
  let callback

  beforeEach(() => {
    server = sinon.fakeServer.create()
    server.respondWith('GET',
      '/some/article/comments.json',
      [
        200,
        { 'Content-Type': 'application/json' },
        '[{ "id": 12, "comment": "Hey there" }]'
      ])
    server.respondWith([404, {}, 'not found article'])

    callback = sinon.spy()
  })

  afterEach(() => {
    server.restore()
  })

  it('calls back with right results', () => {
    myLib.getCommentsFor('/some/article', callback)

    server.respond()
    expect(callback.called).toEqual(true)
    expect(server.requests.length).toBeGreaterThan(0)

    sinon.assert.calledWith(callback, [{ id: 12, comment: 'Hey there' }])
  })

  it('calls nothing if not responding ok request', () => {
    myLib.getCommentsFor('/another/article', callback)

    server.respond()
    expect(callback.called).toEqual(true)
    expect(server.requests.length).toBeGreaterThan(0)
    sinon.assert.calledWith(callback, 'not found article')
  })
})
