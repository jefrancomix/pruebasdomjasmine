/* global XMLHttpRequest */
document.addEventListener('DOMContentLoaded', () => {
  const form = document.querySelector('form')
  const result = document.querySelector('#validationResult')
  form.addEventListener('submit', e => {
    e.preventDefault()
    if (validateCardDetails(form)) {
      result.className = 'good'
    } else {
      result.className = 'bad'
    }
  })
})

const validateCardDetails = (form) => {
  const formArray = [
    form.querySelector('#name'),
    form.querySelector('#cvv')
  ]
  const nombre = formArray[0].value
  const cvv = formArray[1].value
  return validarCvv(cvv) && validarNombre(nombre)
}

const validarCvv = whatever => true

const validarNombre = (nombre) => {
  if (nombre === '') {
    return false
  }
  if (nombre.indexOf(' ') === 0) {
    return false
  }
  const arrayWords = nombre.split(' ').filter(palabra => palabra !== '')
  if (arrayWords.length < 2) {
    return false
  }
  if (arrayWords.filter(w => w.length <= 30).length !== arrayWords.length) {
    return false
  }
  const matchUpperCase = /[A-Z]/.test(nombre)
  const matchLowerCase = /[a-z]/.test(nombre)
  if (!(matchUpperCase && matchLowerCase)) {
    return false
  }
  return true
}
/* eslint-disable no-unused-vars */
const myLib = {
  getCommentsFor: function (article, callback) {
    const xhr = new XMLHttpRequest()
    xhr.open('GET', `${article}/comments.json`, false)
    xhr.setRequestHeader('Content-Type', 'application/json')
    xhr.send()
    if (xhr.status === 200) {
      console.log(xhr.response)
      callback(JSON.parse(xhr.response))
    } else if (xhr.status === 404) {
      callback(xhr.responseText)
    } else {
      console.dir(xhr)
    }
  }
}
